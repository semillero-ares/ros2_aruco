# Node v4l2_camera does not load the camera calibration correctly

from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ns = "/camera" 
    return LaunchDescription([
        Node(
            package='v4l2_camera',
            executable='v4l2_camera_node',
            output="screen",
            name='camera_stream',
            namespace=ns,
            parameters=[{"camera_info_url":"file:///home/jammy/ares_ws/src/calibrationdata/ost.yaml"}],
            remappings=[
                ('/camera/camera_info', '/camera/camera_info'),
                ('/camera/image', '/camera/image_raw')]
        ),
        Node(
            package="tf2_ros",
            executable="static_transform_publisher",
            arguments=["0","0","0","0","0","0","world","camera"],
            output="screen")

    ])

    
