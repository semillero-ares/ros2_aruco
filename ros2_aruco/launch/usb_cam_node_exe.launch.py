from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ns = "/camera" 
    return LaunchDescription([
        Node(
            package='usb_cam',
            executable='usb_cam_node_exe',
            output="screen",
            name='camera_stream',
            namespace=ns,
            parameters=[{
                "camera_info_url":"file:///home/jammy/ares_ws/src/calibrationdata/ost.yaml",
                "framerate":30.0,
                "camera_name":"narrow_stereo"
                }],
            remappings=[
                ('/camera/camera_info', '/camera/camera_info'),
                ('/camera/image', '/camera/image_raw')]
        ),
        Node(
            package="tf2_ros",
            executable="static_transform_publisher",
            arguments=["0","0","0","0","0","0","world","camera"],
            output="screen")

    ])

    
