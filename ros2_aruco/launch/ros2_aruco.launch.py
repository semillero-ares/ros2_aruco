from launch import LaunchDescription
from launch_ros.actions import Node

from ament_index_python.packages import get_package_share_directory
pkg_dir = get_package_share_directory('ros2_aruco')

def generate_launch_description():
    ns = "/camera" 
    return LaunchDescription([
        Node(
            package='usb_cam',
            executable='usb_cam_node_exe',
            output="screen",
            name='camera_stream',
            namespace=ns,
            parameters=[{
                "camera_info_url":"file://{0}/camera_calibration.yaml".format(pkg_dir),
                "framerate":30.0,
                "camera_name":"camera"
                }],
            remappings=[
                ('/camera/camera_info', '/camera/camera_info'),
                ('/camera/image', '/camera/image_raw')]
        ),
        Node(
            package='ros2_aruco',
            executable='aruco_node',
            output="screen",
            name='aruco',
            namespace=ns,
            parameters=[{
                "camera_frame":"camera"
                }],
        ),
        Node(
            package="tf2_ros",
            executable="static_transform_publisher",
            arguments=["0","0","0","0","0","0","world","camera"],
            output="screen")
    ])

    
