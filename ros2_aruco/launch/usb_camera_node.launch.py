# I did not test the node usb_camera_driver_node

from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ns = "/camera" 
    return LaunchDescription([
        Node(
            package='usb_camera_driver',
            executable='usb_camera_driver_node',
            output="screen",
            namespace=ns,
            parameters=[{"camera_calibration_file": "file:///home/spragunr/.ros/camera_info/camera.yaml"}],
            remappings=[
                ('/camera/camera_info', '/camera/camera_info'),
                ('/camera/image', '/camera/image_raw')]
        ),
        Node(
            package="tf2_ros",
            executable="static_transform_publisher",
            arguments=["0","0","0","0","0","0","world","camera"],
            output="screen")

    ])

    
